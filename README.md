# EEE3088 Project

## Mini Weather Station

## Introduction
A repo that contains all the collaboratives work in designing the Enviro Sensing HAT project, The Mini Weather Station. The Mini Weather Station is able to identify significant data such as Temperature, Air Pressure and Humidity and display this data to a virtual weather station on the user’s PC, laptop or similar device.

## Use Cases
- Pressure sensor
- Temperature sensor
- Humidity sensor
- Digital user interface, "Mini Weather Station", that displays all data on laptops and other devices
- CubeCode IDE (C language)


## What you will need 
-  A STM32 microcontroller (on the STM32F051R8T6 Discovery Board)
-  Li-Ion battery (5V)
- 2 x micro USB cable (for Microcontroller and to charge the battery through charging circuit).
- A laptop (interface with microcontroller and hat)

## Hardware Setup
- To interface, plug the micro USB into the USB port on the Discovery Board and use the Weather App to read data from the Hat Sensor
- To charge the battery on the HAT, simply plug a USB into the charging circuit USB port indicated on the PCB 
- To use an alternate power while the battery is charging, plug in a micro USB to the relevant port indicated on the PCB

## Contribute to MiniWS

Welcome to the MiniWS team,

We are always looking for ideas you this project! if you would like to point out any flaws in our design, or simply add to it, feel free to raise your concerns on Gitlab. 

For all contributions, please send an email to contributions@miniws.com with your idea and supporting documentation detailing your changes/additions.
A member of our team will get back to you within one business week. 
But, in the case of wanting to get ahead with changes that you think are pertinent, we are happy for you to get going with development. In this case, please submit a pull request for approval. 
Once your contribution is finished, please submit a push request for final review.  

*Bugs and Fixes*

If you encounter issues with this Repo, please log an issue here: issues@miniws.com
Please include the pertinent details in a short description as well as schematics or code you think will be relevant. 
We will endeavour to solve them as soon as possible.  

Format: 
- Changes to MiniWS design Schematic and PCB layout must be done in KiCad.  
- CubeIDE is preferable for all software related queries.  
- English (UK) is preferable for all documentation. 


## Support 
Get in touch with us via email at:
miniWS@gmail.com

## Authors and acknowledgment
C Habib,
D Koshy,
J Pooran
Big Thank you to the (always helful) Divyan Chetty, Adie Winter, Justin Pead and all our third year friends in the EEE department who helped us along the way. 

## Licence 
Valuing your input, this repo is licenced through: 

GNU GENERAL PUBLIC LICENSE Version 3

## Project status
PCB complete. Currently in the testing stage. 
